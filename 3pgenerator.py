from docx.document import Document
from docx.oxml.table import CT_Tbl
from docx.oxml.text.paragraph import CT_P
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph
import docx
import openpyxl
import xlsxwriter
import re


foundModels=[]
tables=[]
def iter_block_items(parent):
    """
    Check every item for a table occurance
    """
    if isinstance(parent, Document):
        parent_elm = parent.element.body
    elif isinstance(parent, _Cell):
        parent_elm = parent._tc
    else:
        raise ValueError("something's not right")

    for child in parent_elm.iterchildren():
        if isinstance(child, CT_Tbl):
            yield Table(child, parent)
            table = Table(child, parent)
            for row in table.rows:
                 for cell in row.cells:
                     pattern = re.compile('w:fill=\"(\S*)\"')
                     match = pattern.search(cell._tc.xml)
                     result = match.group(1)
                     if result=="ff9900":
                         """
                         this table can be conserned as a model description
                         process the table
                         """
                         foundModels.append(table)
                         break

class genModelMember:
    def __init__(self,valueName,optional,type):
        self.valueName=valueName
        self.optional=optional
        self.type=type


class genModel:
    def __init__(self,name):
        self.name=name
        self.genModelMembers=[]
    def setModelMembers(self,modelmembers):
        self.genModelMembers=modelmembers



def main():
    modelDict={}
    pre_cell=None
    doc = docx.Document('C:\\Users\\u\\Desktop\\ytb.docx')
    for block in iter_block_items(doc):
        if block.style.name == 'Table Grid':
            print("something is wrong")


    for table in foundModels:
        tables.append(table)

    dictTables = dict((idx,table) for idx,table in enumerate(tables))
    # for table in tables:
    #     print("\n")
    #     for row in table._rows:
    #         for cell in row.cells:
    #             current_cell=cell
    #             if current_cell==pre_cell:
    #                 continue
    #             pre_cell=current_cell
    #             print (cell.paragraphs[0].text)
    numTables=len(dictTables)
    genModels=[]
    genMod=[]
    for i in range(numTables):
        genModels=[]
        gm = genModel(dictTables[i].rows[0].cells[0].paragraphs[0].text)
        numRows=len(dictTables[i].rows)
        for x in range(1,numRows,1):
            gmm = genModelMember(dictTables[i].rows[x].cells[0].paragraphs[0].text,dictTables[i].rows[x].cells[1].paragraphs[0].text,dictTables[i].rows[x].cells[2].paragraphs[0].text)
            genModels.append(gmm)
        # print(genModels)
        gm.setModelMembers(genModels)
        genMod.append(gm)
    # for val in genMod:
    #     print("modelname: "+val.name)
    #     # print(val.genModelMembers)
    #     for gmm in val.genModelMembers:
    #             print(gmm.valueName,'////',gmm.optional,'/////',gmm.type)
    generateDart(genMod)



def generateDart(models):
    for model in models:
        file = open("C:\\Users\\u\\Desktop\\models\\"+model.name+"Model.dart", "w")
        file.write("class "+model.name+"Model  {\n")
        for value in model.genModelMembers:
            if ((value.type=="")&(value.optional=="PK")):
                file.write("  late int "+value.valueName+";\n")
            elif ((value.type=="")&(value.optional=="FK - Required")):
                file.write("  late int "+value.valueName+";\n")
            elif ((value.type=="")&(value.optional=="FK - Optional")):
                    file.write("  int? "+value.valueName+";\n")
            elif((value.type=="Date")&(value.optional=="Required")):
                file.write("  late String "+value.valueName+";\n")
            elif((value.type=="Date")&(value.optional=="Optional")):
                file.write("  String? "+value.valueName+";\n")
            elif((value.type!="")&(value.optional=="Required")):
                file.write("  late "+value.type+" "+value.valueName+";\n")

            elif((value.type!="")&(value.optional=="Optional")):
                file.write("  "+value.type+"? "+value.valueName+";\n")
        file.write("\n")

        file.write("  "+model.name+"Model(\n{\n")
        for value in model.genModelMembers:
            if((value.optional=="PK")|(value.optional=="FK - Required")|(value.optional=="Required")):
                file.write("      required this."+value.valueName+",\n")
            else:
                file.write("      this."+value.valueName+",\n")
        file.write("});\n\n")
        file.write("  "+model.name+"Model.fromJson(Map<String,dynamic> json) {\n")
        for value in model.genModelMembers:
            file.write("    "+value.valueName+" = json['"+value.valueName+"'];\n")
        file.write("  }\n\n")
        file.write("  Map<String, dynamic> toJson() {\n")
        file.write("    final Map<String, dynamic> data = new Map<String, dynamic>();\n")
        for value in model.genModelMembers:
            file.write("    data['"+value.valueName+"'] = this."+value.valueName+";\n")
        file.write("    return data;\n")
        file.write("  }\n")
        file.write("}\n")
        file.close()



if __name__ == "__main__":
    main()
